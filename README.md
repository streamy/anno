anno
====

A simple UDP broadcast based announce & discover library.

This is intended for different instances of an application on the same network to announce and discover one another.
