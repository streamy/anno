package anno

import (
	"context"
	"fmt"
	"sync"
	"testing"
	"time"
)

const (
	appID = "anno-test"
	port  = 6161
)

func TestAnno(t *testing.T) {
	data := []byte("000")
	ctx, cancel := context.WithCancel(context.Background())

	// schedule a cancel
	go func() {
		time.Sleep(100 * time.Millisecond)
		cancel()
	}()

	if err := testAnno(ctx, data, [][]byte{data}, t.Logf); err != nil {
		t.Error(err)
	}
}

func TestAnnoMulti(t *testing.T) {

	datas := [][]byte{
		[]byte("123"),
		[]byte("456"),
		[]byte("789"),
	}
	ctx, cancel := context.WithCancel(context.Background())

	// schedule a cancel
	go func() {
		time.Sleep(3 * time.Second)
		cancel()
	}()

	var wg sync.WaitGroup
	errs := make(chan error, len(datas))
	for _, data := range datas {
		wg.Add(1)
		go func(data []byte) {
			defer wg.Done()
			if err := testAnno(ctx, data, datas, t.Logf); err != nil {
				errs <- err
			}
		}(data)
	}

	wg.Wait()
	close(errs)

	for err := range errs {
		if err != nil {
			t.Error(err)
		}
	}

}

func testAnno(ctx context.Context, myData []byte, wantedData [][]byte, logf func(string, ...interface{})) error {

	// discover
	discovered := make(chan Discovered, 1024)

	discErrs := make(chan error, 1)
	go func() {
		discErrs <- Discover(ctx, DiscoverConf{appID, port}, discovered)
	}()

	// allow discovering to begin
	time.Sleep(5 * time.Millisecond)

	// announce
	if err := Announce(ctx, AnnounceConf{appID, port, myData, 10 * time.Second}); err != nil {
		return err
	}

	if err := <-discErrs; err != nil {
		return err
	}

	if len(discovered) == 0 {
		return fmt.Errorf("%s : nothing discovered", myData)
	}
	close(discovered)

	wanted := make(map[string]bool)
	for _, w := range wantedData {
		wanted[string(w)] = false
	}

	for disc := range discovered {
		logf("%s : discovered from %v: %v\n", myData, disc.Address, string(disc.Data))
		_, ok := wanted[string(disc.Data)]
		if !ok {
			return fmt.Errorf("%s : got unexpected %s from %s", myData, disc.Data, disc.Address)
		}
		wanted[string(disc.Data)] = true
	}
	for w, got := range wanted {
		if !got {
			return fmt.Errorf("%s : expected %s but didn't get it", myData, w)
		}
	}

	return nil

}
