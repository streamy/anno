package anno

import (
	"bytes"
	"context"
	"errors"
	"fmt"
	"log"
	"net"
	"syscall"
	"time"

	"golang.org/x/sys/unix"
)

// AnnounceConf is the configration used to announce this node
type AnnounceConf struct {
	ApplicationID  string
	Port           int
	Data           []byte
	AnnouncePeriod time.Duration
}

var lc = net.ListenConfig{Control: func(network, address string, c syscall.RawConn) error {
	var err error
	if errC := c.Control(func(fd uintptr) {
		err = unix.SetsockoptInt(int(fd), syscall.SOL_SOCKET, unix.SO_REUSEPORT, 1)
	}); errC != nil {
		return errC
	}
	return err
}}

// Announce begins announcing a node, based on the provided AnnounceConf and continues doing so until the provided ctx is done.
func Announce(ctx context.Context, conf AnnounceConf) error {

	announcePeriod := conf.AnnouncePeriod

	conn, err := lc.ListenPacket(ctx, "udp", fmt.Sprintf("0.0.0.0:%d", conf.Port))
	if err != nil {
		return err
	}
	defer conn.Close()

	dsts, err := broadcastAddrs()
	if err != nil {
		return err
	}

	tick := make(chan struct{})

	go func() {
		tick <- struct{}{}

		t := time.NewTicker(announcePeriod)
		for {
			select {
			case <-t.C:
				tick <- struct{}{}
			case <-ctx.Done():
				t.Stop()
				return
			}
		}
	}()

	bytes := make([]byte, 0, len(conf.ApplicationID)+len(conf.Data))
	bytes = append(bytes, []byte(conf.ApplicationID)...)
	bytes = append(bytes, conf.Data...)

	for {
		select {
		case <-ctx.Done():
			return nil

		case <-tick:
			//log.Printf("announcing to addresses: %v", dsts)
			for _, ip := range dsts {
				dst := &net.UDPAddr{IP: ip, Port: conf.Port}

				conn.SetWriteDeadline(time.Now().Add(time.Second))
				_, err := conn.WriteTo(bytes, dst)
				conn.SetWriteDeadline(time.Time{})

				if err, ok := err.(net.Error); ok && err.Timeout() {
					return err
				}

				if err, ok := err.(net.Error); ok && err.Temporary() {
					log.Printf("transient error : %s\n", err.Error())
				}

				if err != nil {
					return err
				}
			}
		}
	}
}

// Discovered represents a discovered node
type Discovered struct {
	// Address is the address of the discovered node
	Address net.IP
	// Data is application dependent data from the discovered node
	Data []byte
}

// DiscoverConf is the configration used to discover nodes
type DiscoverConf struct {
	ApplicationID string
	Port          int
}

// Discover begins attempting to discover nodes, based on the provided DiscoverConf and continues doing so until the provided ctx is done.
func Discover(ctx context.Context, conf DiscoverConf, discovered chan<- Discovered) error {
	conn, err := lc.ListenPacket(ctx, "udp", fmt.Sprintf("0.0.0.0:%d", conf.Port))
	if err != nil {
		return err
	}
	udpConn := conn.(*net.UDPConn)

	defer conn.Close()

	errs := make(chan error, 1)
	go func() {

		appIDBytes := []byte(conf.ApplicationID)

		buf := make([]byte, 1024)

		for {
			i, _, _, addr, err := udpConn.ReadMsgUDP(buf, nil)
			if err != nil {
				errs <- err
				return
			}
			if bytes.HasPrefix(buf[0:i], appIDBytes) {
				discovered <- Discovered{Address: addr.IP, Data: dup(buf[len(appIDBytes):i])}
			}
		}
	}()

	select {
	case err := <-errs:
		return err
	case <-ctx.Done():
		conn.SetReadDeadline(time.Now())
		<-errs
		return nil
	}

}

func dup(in []byte) []byte {
	out := make([]byte, len(in))
	copy(out, in)
	return out
}

func broadcastAddrs() ([]net.IP, error) {
	ifaces, err := net.Interfaces()
	if err != nil {
		return nil, err
	}

	var dsts []net.IP

	for _, iface := range ifaces {
		//log.Printf("considering interface %s . Flags : %v\n", iface.Name, iface.Flags)
		if iface.Flags&net.FlagUp != 0 {
			ifAddrs, err := iface.Addrs()
			if err != nil {
				return nil, err
			}
			for _, ifAddr := range ifAddrs {
				ipNet, ok := ifAddr.(*net.IPNet)
				if !ok {
					continue
				}
				if !ipNet.IP.IsGlobalUnicast() {
					continue
				}
				if len(ipNet.IP) < 4 {
					continue
				}
				ip4 := ipNet.IP.To4()
				if ip4 == nil {
					continue
				}

				baddr := broadcastAddress(ip4, ipNet.Mask)
				dsts = append(dsts, baddr)
			}
		}
	}

	if len(dsts) == 0 {
		return nil, noAddrs
	}

	return dsts, nil
}

var noAddrs = errors.New("no addresses to broadcast to")

func broadcastAddress(ip net.IP, mask net.IPMask) net.IP {
	if len(ip) != len(mask) {
		panic("bug")
	}
	bcastAddr := make([]byte, len(ip))
	copy(bcastAddr, ip)

	for i := range ip {
		bcastAddr[i] = ip[i] | ^mask[i]
	}
	return bcastAddr
}
