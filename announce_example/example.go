package main

import (
	"context"
	"crypto/rand"
	"encoding/hex"
	"flag"
	"fmt"
	"log"
	"os"
	"time"

	"gitlab.com/streamy/anno"
)

var (
	appID = "example-app"
	port  = 6161
	mode  = flag.String("mode", "", "mode. (announce or discover)")
)

func main() {

	flag.Parse()

	if false {
		flag.Usage = func() {
			fmt.Fprintf(os.Stderr, "usage: example [options] \n")
			flag.PrintDefaults()
			os.Exit(2)
		}
	}

	switch *mode {
	case "announce":
		if err := announce(genId()); err != nil {
			log.Fatal(err)
		}
	case "discover":
		if err := discover(); err != nil {
			log.Fatal(err)
		}
	default:
		flag.Usage()
	}

}

func genId() string {
	var buf [8]byte
	var dest [16]byte
	if _, err := rand.Read(buf[:]); err != nil {
		panic(err)
	}

	hex.Encode(dest[:], buf[:])
	return string(dest[:])
}

func announce(id string) error {
	return anno.Announce(context.Background(), anno.AnnounceConf{appID, port, []byte(id), time.Second})
}

func discover() error {

	discovered := make(chan anno.Discovered, 1024)

	go func() {
		for disc := range discovered {
			log.Printf("discovered %s at %v\n", disc.Data, disc.Address)
		}
	}()

	return anno.Discover(context.Background(), anno.DiscoverConf{appID, port}, discovered)

}
